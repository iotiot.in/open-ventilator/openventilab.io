---
id: whatsapp
title: Join WhatsApp Group
---

### Contributors can join whatsapp groups here :
* Are you an electronics /mechanical engineer? 
Can help with design improvements ? Please join
[bit.ly/engg4venti1](bit.ly/engg4venti1)

* Are you a doctor?
Can help with verifying  specifications and guiding us ? Please join
[bit.ly/doctors4venti1](bit.ly/doctors4venti1)

* Are you a biomedical manufacturer? 
Can help with manufacture and design of open source ventilator ? Please join
[bit.ly/biomed4venti1](bit.ly/biomed4venti1)

* Are you a dealer/distributor/trader of medical products?
Can help us know supply of products .We will send you the orders from hospital Please join
[bit.ly/channels4venti1](bit.ly/channels4venti1)

*  Are you a student/volunteers/enthusiasts?
Can take up random tasks as they come .Please join 
[bit.ly/ventihelp1](bit.ly/ventihelp1)

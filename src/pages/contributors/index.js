import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

import Image from '@theme/IdealImage';


const withImg = [
  {
    thumbnail: require('./data/nik.jpg'),
    name: 'Nikhil Bhaskaran',
    num: '0',
    title: 'Lead Project Servant',
    text: (
      <>
        Nik has always been active with social projects .Got a call from hospital about predicted ventilator shortage .It drew him into action ,he quickly deployed his  team and resources into action and in a short period has been able to get connected to reputed industry ,academia and experts who are helping him drive this project .I request and welcome all to support this initiative.
      </>
    ),
   linkedin: 'https://www.linkedin.com/in/nikhilbhaskaran/',
  },

  {
    thumbnail: require('./data/rsz_nick.jpg'),
    name: 'Nick Booker',
    num: '0',
    title: 'Co-Founder OpenBreath.Tech',
    text: (
      <>
        Nick’s main work prior to COVID-19 was as Co-Founder of IndoGenius. He has been living and working in India for over a decade, teaching students from around the world about innovation in India. He has worked with clients incl. dozens of international universities, DFAT Australia, the European Commission, the U.S. Department of State, MAE France, and the Government of India.
      </>
    ),
   linkedin: 'https://www.linkedin.com/in/indogenius/',
  },

  {
    thumbnail: require('./data/mayank.jpg'),
    name: 'Dr Mayank Verma',
    num: '0',
    title: 'Technical Documentation Head',
    text: (
      <>
        Mayank is an Honors Graduate in Mechanical Engineering and a PhD from IIT Kanpur. He is currently serving as Scientific Officer with the Indian Atomic Energy Regulatory Board, where he is in-charge of the Regulatory reviews of India's first indigenous 700 MWe PHWR type Nuclear Power Plants.
      </>
    ),
  },

];

const withImg2 = [
  {
    thumbnail: require('./data/blank.png'),
    name: 'N K Bedarkar',
    num: '1',
    title: 'Advisor-Electronics and Manufacturing',
    text: (
      <>
	Advice related to the product design  , control circuits ,measurement
        </>
    ),
  },

  {
    thumbnail: require('./data/blank.png'),
    name: 'Aditi',
    num: '0',
    title: 'Mediator',
    text: (
      <>
	Just helping figure things out where ever I can.
        </>
    ),
  },

  {
    thumbnail: require('./data/ruturaj.jpg'),
    name: 'Ruturaj Shinde',
    num: '0',
    title: 'Lead - Maharashtra Group',
    text: (
      <>
        Working as Lead for Maharashtra region in order to track the supply and Demand of medical equipments during this crisis of Covid-19 across the state. 
 </>
    ),
  },
 
];

const withImg3 = [
  {
    thumbnail: require('./data/dipti.jpg'),
    name: 'Dipti',
    num: '0',
    title: 'Gujarat Lead',
    text: (
      <>
       OpenVenti project- Volunteer contributor- Gujarat Region Lead
 </>
    ),
  },
 
];

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Contributors<head />">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">Our Contributors</h1>
          <p className="hero__subtitle">Thanks to everyone for their contribution.</p>
          <p className="hero__subtitle">This Project is thriving due to contributions made by these people.</p>
        </div>
      </header>
      <main>
        <div
          className={classnames(
            styles.section,
            styles.sectionAlt,
          )}>
          <div className="container">
            <div className="row">
              {withImg.map(quote => (
                <div className="col" key={quote.name} >
                  <div className="avatar avatar--vertical margin-bottom--sm">
                    <Image
                      alt={quote.name}
                      className="avatar__photo avatar__photo--xl"
                      img={quote.thumbnail}
                      style={{overflow: 'hidden'}}
                    />
                    <div className="avatar__intro padding-top--sm">
                      <h4 className="avatar__name">{quote.name}</h4>
                      <small className="avatar__subtitle">{quote.title}</small>
                    </div>
                  </div>
		
		  <a href={quote.linkedin}>
            		<p className="text--center text--italic padding-horiz--md">
                        	LinkedIn Profile
            	  	</p>
         	 </a>
                  <p className="text--center text--italic padding-horiz--md">
                    {quote.text}
                  </p>
                </div>
              ))}
            </div>
            <div className="row">
              {withImg2.map(quote => (
                <div className="col" key={quote.name} >
                  <div className="avatar avatar--vertical margin-bottom--sm">
                    <Image
                      alt={quote.name}
                      className="avatar__photo avatar__photo--xl"
                      img={quote.thumbnail}
                      style={{overflow: 'hidden'}}
                    />
                    <div className="avatar__intro padding-top--sm">
                      <h4 className="avatar__name">{quote.name}</h4>
                      <small className="avatar__subtitle">{quote.title}</small>
                    </div>
                  </div>
                  <p className="text--center text--italic padding-horiz--md">
                    {quote.text}
                  </p>
                </div>
              ))}
            </div>
            <div className="row">
              {withImg3.map(quote => (
                <div className="col" key={quote.name} >
                  <div className="avatar avatar--vertical margin-bottom--sm">
                    <Image
                      alt={quote.name}
                      className="avatar__photo avatar__photo--xl"
                      img={quote.thumbnail}
                      style={{overflow: 'hidden'}}
                    />
                    <div className="avatar__intro padding-top--sm">
                      <h4 className="avatar__name">{quote.name}</h4>
                      <small className="avatar__subtitle">{quote.title}</small>
                    </div>
                  </div>
                  <p className="text--center text--italic padding-horiz--md">
                    {quote.text}
                  </p>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className={classnames('hero hero--primary-dark', styles.heroBanner)}>
          <div className="container">
              <Link
              className={classnames(
              		'button button--outline button--secondary button--lg',
              		styles.indexCtas,
                  )}
              to={useBaseUrl('docs/contributors')}>
              More...
              </Link>                
          </div>
        </div>
      </main>
    </Layout>
  );
}

export default Home;

module.exports = {
  title: 'Open Venti',
  tagline: 'Open Source ventilator project for COVID-19 patients.',
  url: 'https://iotiotdotin.gitlab.io',
  baseUrl: '/openventi/',
  favicon: 'img/favicon.ico',
  organizationName: 'iotiotdotin', // Usually your GitHub org/user name.
  projectName: 'openventi', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Open Venti',
      logo: {
        alt: 'Open Venti Logo',
        src: 'img/logo.png',
      },
      links: [
        {to: 'blog', label: 'Blog', position: 'left'},
        {to: 'contributors', label: 'Contributors', position: 'left'},
        {
          href: 'https://iotiotdotin.gitlab.io/openventi/docs/whatsapp/',
          label: 'WhatsApp',
          position: 'right',
        },
        {
          href: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/wikis/home',
          label: 'Wiki',
          position: 'left',
        },
        {
          href: 'http://3.7.55.159/',
          label: 'Check Stock',
          position: 'left',
        },
        {
          href: 'https://gitlab.com/iotiotdotin/open-ventilator/resources/-/wikis/home',
          label: 'Resources',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Community',
          items: [
            {
              label: 'Gitter',
              href: 'https://gitter.im/iotiotopenventi/community',
            },
          ],
        },
        {
          title: 'Social',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} IoTIoT.in.`,
    },
  },
  plugins: [
      [
        '@docusaurus/plugin-ideal-image',
        {
          quality: 70,
          max: 1030, // max resized image's size.
          min: 640, // min resized image's size. if original is lower, use that size.
          steps: 2, // the max number of images generated between min and max (inclusive)
        },
      ],
    ],
    presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/iotiotdotin/openventi/-/edit/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
